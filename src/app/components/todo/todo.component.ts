import { Component, OnInit, ViewChild } from '@angular/core';
import { Todo } from '../../models/todo/Todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  todos: Todo[];
  todo: Todo = {
    id: '',
    title: '',
    text: '',
    complete: false
  };
  @ViewChild('form') form;

  constructor() { }

  ngOnInit() {
    this.todos = [
      {
        id: '0',
        title: 'Первый таск',
        text: 'Первый текст',
        complete: false
      },
      {
        id: '1',
        title: 'Второй таск',
        text: 'Второй текст',
        complete: true
      },
      {
        id: '2',
        title: 'Третий таск',
        text: 'Третий текст',
        complete: false
      },

    ];
  }
  deleteTask(id: string) {
    this.todos = this.todos.filter(task => task.id !== id);
  }
  completeTask(task: Todo) {
    let complete = task.complete;
    task.complete = !complete;
  }
  addTodo() {
    const newTodo = {
      id: this.todos.length.toString(),
      title: this.todo.title,
      text: this.todo.text,
      complete: false
    };
    this.todos.push(newTodo);

    this.form.reset();
  }
}
